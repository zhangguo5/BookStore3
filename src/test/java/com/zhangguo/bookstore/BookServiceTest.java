package com.zhangguo.bookstore;

import com.zhangguo.bookstore.entity.Book;
import com.zhangguo.bookstore.service.BookService;
import org.junit.jupiter.api.Test;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class BookServiceTest {
    @Autowired
    BookService bookService;

    @Test
    public void selectBooksTest(){
        Book book=new Book(0,"C","i",0,null,"",null);
        System.out.println(bookService.selectBooks(book,1,10));
    }
    @Test
    public void deleteBookTest(){
        System.out.println(bookService.deleteBook(1074));
    }

    @Test
    public void addBookTest(){
        Book book=new Book(0,"test","test",17,new Date(),"12345678",null);
        System.out.println(bookService.addBook(book));
    }

    @Test
    public void selectBookTest(){
        Book book=bookService.selectBook(1084);
        System.out.println(book);
    }

    @Test
    public void editBook(){
        Book book=bookService.selectBook(1087);
        book.setTitle("Spring Boot实战2");
        book.setAuthor("zhangguo2");
        book.setCategoryId(16);
        book.setPublishDate(new Date());
        int result = bookService.editBook(book);
        System.out.println(result);
    }
}
