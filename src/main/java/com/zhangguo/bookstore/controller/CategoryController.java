package com.zhangguo.bookstore.controller;

import com.zhangguo.bookstore.entity.Category;
import com.zhangguo.bookstore.service.CategoryService;
import com.zhangguo.bookstore.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**图书分类控制器*/
@RestController
@RequestMapping("/api")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("/categories")
    public R findAll(){
        List<Category> categories = categoryService.findAll();
        return R.ok(categories);
    }
}
