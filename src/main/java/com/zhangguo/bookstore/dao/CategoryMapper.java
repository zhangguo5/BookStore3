package com.zhangguo.bookstore.dao;

import com.zhangguo.bookstore.entity.Category;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**图书分类映射接口*/
@Mapper
public interface CategoryMapper {
    /**获取所有分类*/
    public List<Category> findAll();
}
