package com.zhangguo.bookstore.dao;

import com.zhangguo.bookstore.entity.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**图书映射接口*/
@Mapper
public interface BookMapper {
    /**获取图书根据查询条件,多条件组合*/
    public List<Book> selectBooks(Book book);
    /**删除图书根据编号*/
    public int deleteBook(int id);
    /**添加图书*/
    public int addBook(Book book);
    /**查询图书通过编号*/
    public Book selectBook(int id);
    /**修改图书*/
    public int editBook(Book book);
}
