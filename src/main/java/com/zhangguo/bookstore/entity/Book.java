package com.zhangguo.bookstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**图书*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    /**编号*/
    private int id;
    /**书名*/
    private String title;
    /**作者*/
    private String author;
    /**分类编号*/
    private int categoryId;
    /**出版日期*/
    private Date publishDate;
    /**书号*/
    private String isbn;
    /**分类*/
    private Category category;
}
