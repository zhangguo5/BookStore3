package com.zhangguo.bookstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**图书分类*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    /**编号*/
    private int id;
    /**书名*/
    private String name;
    /**状态*/
    private int state;
}
