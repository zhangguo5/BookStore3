package com.zhangguo.bookstore.service.impl;

import com.zhangguo.bookstore.dao.CategoryMapper;
import com.zhangguo.bookstore.entity.Category;
import com.zhangguo.bookstore.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**图书分类服务实现类*/
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryMapper categoryMapper;

    @Override
    public List<Category> findAll() {
        return categoryMapper.findAll();
    }
}
