package com.zhangguo.bookstore.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhangguo.bookstore.dao.BookMapper;
import com.zhangguo.bookstore.entity.Book;
import com.zhangguo.bookstore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**图书服务实现类*/
@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookMapper bookMapper;

    @Override
    public PageInfo<Book> selectBooks(Book book, int pageNum, int pageSize) {
        //开始分页，指定页码与每页记录数
        PageHelper.startPage(pageNum,pageSize);
        //执行查询，请求会被分页插件拦截
        List<Book> books = bookMapper.selectBooks(book);
        //返回分页对象与数据
        return new PageInfo<Book>(books);
    }

    @Override
    public int deleteBook(int id) {
       return bookMapper.deleteBook(id);
    }

    @Override
    public int addBook(Book book) {
        return bookMapper.addBook(book);
    }

    @Override
    public Book selectBook(int id) {
        return bookMapper.selectBook(id);
    }

    @Override
    public int editBook(Book book) {
        return bookMapper.editBook(book);
    }
}
