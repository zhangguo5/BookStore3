package com.zhangguo.bookstore.service;

import com.zhangguo.bookstore.entity.Category;

import java.util.List;

/**图书分类服务接口*/
public interface CategoryService {
    /**获取所有分类*/
    public List<Category> findAll();
}
