package com.zhangguo.bookstore.service;

import com.github.pagehelper.PageInfo;
import com.zhangguo.bookstore.entity.Book;
import org.springframework.stereotype.Service;

import java.util.List;

/**图书服务接口*/
public interface BookService {
    /**获取图书根据查询条件,多条件组合*/
    public PageInfo<Book> selectBooks(Book book, int pageNum, int pageSize);
    /**删除图书根据编号*/
    public int deleteBook(int id);
    /**添加图书*/
    public int addBook(Book book);
    /**查询图书通过编号*/
    public Book selectBook(int id);
    /**修改图书*/
    public int editBook(Book book);
}
